# Housekeeping
Clear-Host
$pshost               = Get-Host
$pswindow             = $pshost.UI.RawUI
$pswindow.WindowTitle = "Yudu converter"

# Load all modules that we will need
Import-Module -Name ($PSScriptRoot + ("\modules\ConvertFrom-Swf.psm1")) -Force
Import-Module -Name ($PSScriptRoot + ("\modules\Remove-StringSpecialCharacter.psm1")) -Force
Import-Module -Name ($PSScriptRoot + ("\modules\Compress-Images.psm1")) -Force
Import-Module -Name ($PSScriptRoot + ("\modules\New-PdfFromImages.psm1")) -Force
Import-Module -Name ($PSScriptRoot + ("\modules\Unlock-Pdf.psm1")) -Force

# Publication name
Write-Host "Enter the output file name" -ForegroundColor DarkCyan
[string]$pubName = Read-Host

# Publication page count
Write-Host "`r`n"
Write-Host "Enter the total number of pages" -ForegroundColor DarkCyan
Write-Host "This figure should include any supplements which form part of the publication" -ForegroundColor DarkCyan
[string]$totalPages = Read-Host

# Publication URL
Write-Host "`r`n"
Write-Host "Enter the publication URL. It must end with '/resources/':" -ForegroundColor DarkCyan
Write-Host "For example:" -ForegroundColor DarkCyan
Write-Host "http://content.yudu.com/Library/A2kd97/KempstonCallingDecem/resources/" -ForegroundColor DarkCyan
[string]$pubUrl = Read-Host

# Change window title to something more informative
$pswindow.WindowTitle = $pubName + " (Total pages: $totalPages)"

# Create a folder for all activity that takes place based on the publication name
[string]$publicationPath = $PSScriptRoot + '\' + ((Get-Date).ToString('HHmmss'))  + '_' + (Remove-StringSpecialCharacter -String $pubName)
New-Item -ItemType Directory -Force -Path $publicationPath | Out-Null

# Delete any left-over files before we start downloading SWF files
Remove-Item *.png, *.xml, *.swf
For($i=1; $i -le $totalPages; $i++){
    
    # Location of each page and the filename for each page and then download
    [string]$pageUrl             = $pubUrl + "content/" + [string]$i + ".swf"
    [string]$downloadDestination = $publicationPath + '\' + ($i.ToString("000")) + ".swf"

    (New-Object Net.WebClient).DownloadFile($pageUrl, ($downloadDestination))

    # Progress bar for downloading SWF files
    [string]$progressPercentage = "Downloading page $i of $totalPages ~ " + ("{0:P1}" -f ((($i / $totalPages)), 0))
    Write-Progress -Activity "Downloading $pubName" `
        -Status $progressPercentage `
        -PercentComplete (($i / $totalPages) * 100) `
        -CurrentOperation "$pageUrl" `
}

# Launch the module that will convert our pages from SWF to PNG
ConvertFrom-Swf -path $publicationPath -totalPages $totalPages

# Remove SWF files from directory now that conversion is complete
# Then move all converted PNG files from their respective sub-directories up one level
# Once files are moved, delete all empty sub-directories
For ($i=1; $i -le $totalPages; $i++) {

    Remove-Item($publicationPath + '\' + ($i.ToString("000")) + ".swf")
    Move-Item -Path ($publicationPath + '\' + ($i.ToString("000")) + "\*.png") -Destination $publicationPath
    Remove-Item ($publicationPath + '\' + ($i.ToString("000")))
}

# Opportunity to remove advert pages
Write-Host `r`n
Write-Host "If you want, you can now delete any pages that need to be removed." -ForegroundColor Red
Write-Host `r`n

# Calculate file size of all the images we've generated
# Ask user if they want to compress the images
$fileSize = ((Get-ChildItem ($publicationPath + '\*.png') | Measure-Object -Sum Length).Sum / 1MB).toString("0.00")
Write-Host ("Converted " + $totalPages + " files with a total file size of " + $filesize + "MB") -ForegroundColor Red
Write-Host "Would you like to compress your images? Enter Y or N only." -ForegroundColor Red
$compressDecision = Read-Host
$compressDecision = $compressDecision.Trim().ToLower()

If ($compressDecision -eq "yes" -or $compressDecision -eq "y") {
    Write-Host `r`n
    Write-Host "pngquant can compress images and overwrite the original." -foreground "red"
    Write-Host "There are 11 levels of compression. 1 will compress the most/slowest and 11 will compress the least/quickest." -foreground "red"
    Write-Host "The default is level 3. Enter your level of compression" -foreground "red"
    
    try {
        [int]$compressionLevel = Read-Host
    } catch {
        [int]$compressionLevel = 3
    }

    if ($compressionLevel -gt 11 -or $compressionLevel -lt 1) {
        $compressionLevel = 3
    }

    Compress-Images -path $publicationPath -compressionLevel $compressionLevel

    # Compression is now complete so compare old file size with new file size
    # Output results of compression
    $oldSize  = $filesize
    $fileSize = ((Get-ChildItem ($publicationPath + '\*.png') | Measure-Object -Sum Length).Sum / 1MB).toString("0.00")
    Write-Host `r`n
    Write-Host ("Images have been compressed from " + $oldSize + "MB to " + $filesize + "MB.") -ForegroundColor Red
}

Stop-Process -processname "kurst-SWFRenderer" -erroraction "silentlycontinue"

# Build PDF
New-PdfFromImages -path $publicationPath -fileName $pubName

# Unlock PDF
Unlock-Pdf -path $publicationPath -fileName $pubName *>$NULL

# Move PDF from working directory to applicaton directory and delete working directory
Move-Item -Path ($publicationPath + '\*ul.pdf') -Destination $PSScriptRoot -Force -ErrorAction SilentlyContinue
Remove-Item -Path $publicationPath -Force -Recurse

# Reset window title now that process is complete
$pswindow.WindowTitle = "Yudu converter"

# And finally, an on-screen message to end it all
Write-Host "`r`n"
Write-Host "This publication is now complete" -ForegroundColor DarkCyan
