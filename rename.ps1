# Housekeeping
Clear-Host
$pshost               = Get-Host
$pswindow             = $pshost.UI.RawUI
$pswindow.WindowTitle = "Rename image files"

# Prompt user to drag the folder containing files to be renamed
Write-Host "Drag the folder containing images that will be renamed" -ForegroundColor DarkCyan
$inputPath = (Read-Host).Trim() -Replace '"', ''

# Exit if the input path is not for a directory 
If (! ((Get-Item $inputPath) -is [System.IO.DirectoryInfo]) ) {
    Exit
}

# Get all of the images in the folder
$Images =   Get-ChildItem -Path $inputPath `
                          -Recurse `
                          -Include ("*.jpg", "*.png") `
            | Sort-Object { [regex]::Replace($_.Name, '\d+', { $args[0].Value.PadLeft(20) }) }

$filePrefix = "A"
$fileExtension = ".tif"

# Rename all images in the format Axxx.tif
# 3 digit padding with A prefix and tif file extension
$count = 1;

foreach($Image in $Images) {
    $newFileName = $filePrefix +($count.ToString("000")) + $fileExtension
    Rename-Item $Image -NewName $newFileName

    $count ++
}

# Reset window title now that process is complete
$pswindow.WindowTitle = "Rename image files"

# And finally, an on-screen message to end it all
Write-Host "`r`n"
Write-Host "Images have been renamed" -ForegroundColor DarkCyan
