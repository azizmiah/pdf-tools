# Housekeeping
Clear-Host
$pshost               = Get-Host
$pswindow             = $pshost.UI.RawUI
$pswindow.WindowTitle = "Yudu tile converter"

# Load all modules that we will need
Import-Module -Name ($PSScriptRoot + ("\modules\Remove-StringSpecialCharacter.psm1")) -Force
Import-Module -Name ($PSScriptRoot + ("\modules\Merge-Tiles.psm1")) -Force
Import-Module -Name ($PSScriptRoot + ("\modules\Compress-Images.psm1")) -Force
Import-Module -Name ($PSScriptRoot + ("\modules\New-PdfFromImages.psm1")) -Force
Import-Module -Name ($PSScriptRoot + ("\modules\Unlock-Pdf.psm1")) -Force

# Publication name
Write-Host "Enter the output file name" -ForegroundColor DarkCyan
[string]$pubName = Read-Host

# Publication URL
Write-Host "`r`n"
Write-Host "Enter the publication URL. It must be in one of the following three formats:" -ForegroundColor DarkCyan

# http://content.yudu.com/libraryHtml/A42tbj/ChamberlinkMarch2017/
Write-Host "http://content.yudu.com/" -ForegroundColor DarkCyan -NoNewline 
    Write-Host "libraryHtml" -ForegroundColor Yellow -NoNewline
    Write-Host "/A42tbj/ChamberlinkMarch2017/" -ForegroundColor DarkCyan

# http://content.yudu.com/web/69r/0A18qm2/CIEMay2017/html/
Write-Host "http://content.yudu.com/" -ForegroundColor DarkCyan -NoNewline
    Write-Host "web" -ForegroundColor Yellow -NoNewline
    Write-Host "/69r/0A18qm2/CIEMay2017/html/" -ForegroundColor DarkCyan

# http://content.yudu.com/htmlReader/A42wym/AspireJune17/
Write-Host "http://content.yudu.com/" -ForegroundColor DarkCyan -NoNewline
    Write-Host "htmlReader" -ForegroundColor Yellow -NoNewline
    Write-Host "/A42wym/AspireJune17/" -ForegroundColor DarkCyan
[string]$pubUrl = Read-Host

# Publication page count
# Determine using the publication's JSON manifest file
$jsonManifest = Invoke-RestMethod ($pubUrl + "yuduBook.json")
[string]$totalPages = $jsonManifest.yuduBook.pages.Count

# Change window title to something more informative
$pswindow.WindowTitle = $pubName + " (Total pages: $totalPages)"

# Create a folder for all activity that takes place based on the publication name
[string]$publicationPath = $PSScriptRoot + '\' + ((Get-Date).ToString('HHmmss'))  + '_' + (Remove-StringSpecialCharacter -String $pubName)
New-Item -ItemType Directory -Force -Path $publicationPath | Out-Null

# Download the tiles for each page
for($pageNumber = 0; $pageNumber -lt $totalPages; $pageNumber) {

    # Array will store the path to each tile we've downloaded
    $tileLocations = @()

    # 0 to 5 (Y-axis)
    for ($i = 0; $i -le 5; $i++) {

        # 0 to 3 (X-axis)
        for ($j = 0; $j -le 3; $j++) {
            
            # This is the name of each page file
            $pageName = "1-$j-$i"

            # Create sub-directory for each page. The sub-directory stores tiles for that page
            New-Item -ItemType Directory -Force -Path ($publicationPath + "\" + $pageNumber) | Out-Null

            # Location of each page and the filename for each page and then download
            [string]$pageUrl             = $pubUrl + "tiles/" + [string]$pageNumber + "/tile-" + $pageName + ".jpg"
            [string]$downloadDestination = $publicationPath + '\' + $pageNumber + "\" + ($pageName) + ".jpg"

            # Append the path to the currently downloaded file into the tileLocations array
            $tileLocations += $downloadDestination

            (New-Object Net.WebClient).DownloadFile($pageUrl, ($downloadDestination))
        }

    }

    # All the tiles for this page are now downloaded.
    # Let's stitch the tiles into a fully formed page
    # First let's create an array of inputs to feed into the tile stitching command
    $inputRow = @()
    for ($n = 0; $n -lt 6; $n++) {
        
        [string] $v1 = ($n*4) + 0 
        [string] $v2 = ($n*4) + 1
        [string] $v3 = ($n*4) + 2
        [string] $v4 = ($n*4) + 3

        $inputRow += $tileLocations[$v1] + " " + " " + $tileLocations[$v2] + " " + $tileLocations[$v3] + " " + $tileLocations[$v4]
    }

    # Now let's create an array of outputs to feed into the tile stitching command
    $outputRow = @()
    for ($n = 1; $n -le 6; $n++) {
        $outputRow += ( $publicationPath + '\' + $pageNumber + "\" + "row$n.png" )
    }

    # Now let's create the tiled rows using the input and output arrays by stitching horizontally
    for ($n = 0; $n -lt 6; $n++) {
        Merge-Tiles -inputFiles $inputRow[$n] -outputFiles $outputRow[$n] -type "horizontally"        
    }

    # Now let's create a full page out of the rows by stitching vertically
    # Resue the outputRow variable
    $inputRow   = $outputRow[0] + " " + $outputRow[1] + " " + $outputRow[2] + " " + $outputRow[3] + " " + $outputRow[4] + " " + $outputRow[5]
    $outputFile = $publicationPath + '\' + $pageNumber + "\" + ($pageNumber.ToString("000")) + ".png"
    Merge-Tiles -inputFiles $inputRow -outputFiles $outputFile -type "vertically"

    # A fully stitched image has now been created
    # So let's delete all the tile and temp row files
    Remove-Item -Path ( $publicationPath + '\' + $pageNumber + "\" + "*.jpg" ) -Force -Recurse
    Remove-Item -Path ( $publicationPath + '\' + $pageNumber + "\" + "*row*.png" ) -Force -Recurse

    # Now move the fully stitched file up one level to the the publication path
    Move-Item -Path ( $publicationPath + '\' + $pageNumber + "\*.png" ) -Destination $publicationPath -Force -ErrorAction SilentlyContinue

    # Delete the sub-directory which is now empty
    Remove-Item -Path ( $publicationPath + '\' + $pageNumber + "\" ) -Force -Recurse

    # Progress bar for compression 
    [string] $progCurrentPage    = $pageNumber + 1
    [string]$activityDescription = "Downloading and processing page $progCurrentPage of $totalPages. This may take a while."
    [string]$progressPercentage  = [string]( ("{0:P1}" -f ((($progCurrentPage / $totalPages)), 0)) ) + " complete"
    Write-Progress -Activity $activityDescription `
    -Status $progressPercentage `
    -PercentComplete (($progCurrentPage / $totalPages) * 100)

    $pageNumber++
}

# All pages downloaded and stitched
# Ask user if they wish to compress
# Calculate file size of all the images we've generated
# Ask user if they want to compress the images
$fileSize = ((Get-ChildItem ($publicationPath + '\*.png') | Measure-Object -Sum Length).Sum / 1MB).toString("0.00")
Write-Host "`r`n"
Write-Host ("Generated " + $totalPages + " files with a total file size of " + $filesize + "MB") -ForegroundColor Red
Write-Host "Would you like to compress your images? Enter Y or N only." -ForegroundColor Red
$compressDecision = Read-Host
$compressDecision = $compressDecision.Trim().ToLower()

If ($compressDecision -eq "yes" -or $compressDecision -eq "y") {
    Write-Host `r`n
    Write-Host "pngquant can compress images and overwrite the original." -foreground "red"
    Write-Host "There are 11 levels of compression. 1 will compress the most/slowest and 11 will compress the least/quickest." -foreground "red"
    Write-Host "The default is level 3. Enter your level of compression." -foreground "red"
    
    try {
        [int]$compressionLevel = Read-Host
    } catch {
        [int]$compressionLevel = 3
    }

    if ($compressionLevel -gt 11 -or $compressionLevel -lt 1) {
        $compressionLevel = 3
    }

    Compress-Images -path $publicationPath -compressionLevel $compressionLevel

    # Compression is now complete so compare old file size with new file size
    # Output results of compression
    $oldSize = $filesize
    $fileSize = ((Get-ChildItem ($publicationPath + '\*.png') | Measure-Object -Sum Length).Sum / 1MB).toString("0.00")
    Write-Host `r`n
    Write-Host ("Images have been compressed from " + $oldSize + "MB to " + $filesize + "MB.") -ForegroundColor Red
}

# Build PDF
New-PdfFromImages -path $publicationPath -fileName $pubName

# Unlock PDF
Unlock-Pdf -path $publicationPath -fileName $pubName *>$NULL

# Move PDF from working directory to applicaton directory and delete working directory
Move-Item -Path ($publicationPath + '\*ul.pdf') -Destination $PSScriptRoot -Force -ErrorAction SilentlyContinue
Remove-Item -Path $publicationPath -Force -Recurse

# Reset window title now that process is complete
$pswindow.WindowTitle = "Yudu tile converter"

# And finally, an on-screen message to end it all
Write-Host "`r`n"
Write-Host "This publication is now complete" -ForegroundColor DarkCyan

<#
How does this work?
============================
The new HTML5 Yudu reader converts each page into a tiled image that's 6 high, and 4 wide

http://content.yudu.com/libraryHtml/A42hdt/YTirFeb2017/tiles/0/tile-1-1-3.jpg

Each tile is in the format z-x-y where:
~~~~~ the Z corresponds to zoom/depth. The max value is 1.
~~~~~ the X corresponds to width. Anything between 0-3.
~~~~~ the Y corresponds to height. Anything beween 0-5.

So for example: 
~~~~~ the top left corner would be:     1-0-0
~~~~~ the top right corner would be:    1-3-0
~~~~~ the bottom left corner would be:  1-0-5
~~~~~ the botton right corner would be: 1-3-5

This is the base URL:
http://content.yudu.com/libraryHtml/A42hdt/YTirFeb2017/tiles/*/tile-1-1-3.jpg
~~~~~ the * segment after is a page number, always starting from page 0 (i.e. cover page)
~~~~~~~~~~ so therefore, a 20 page pub is actually pages 0-19
#>