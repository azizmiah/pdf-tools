# Housekeeping
Clear-Host
$pshost               = Get-Host
$pswindow             = $pshost.UI.RawUI
$pswindow.WindowTitle = "Batch image downloader"

# Read the links in the links.txt file
$links = Get-Content links.txt

# Counter
$i = 1;

# Create a folder for all the downloads to go into
[string]$publicationPath = $PSScriptRoot + '\' + ((Get-Date).ToString('HHmmss'))  + '_batch_img_dl\'
New-Item -ItemType Directory -Force -Path $publicationPath | Out-Null

# Total files
$totalFiles = (Get-Content links.txt | Measure-Object -Line) 
$totalFiles = $totalFiles.Lines

# Iterate through each line in text file and download
foreach($link in $links)
{
    
    # Progress bar for each download
    [string]$progressPercentage = "Downloading page $i of $totalFiles ~ " + ("{0:P1}" -f ((($i / $totalFiles)), 0))
    Write-Progress -Activity "$link" `
        -Status $progressPercentage `
        -PercentComplete (($i / $totalFiles) * 100) `

    $outputFile = $publicationPath + ([string]$i) + ".jpg"
    $client = New-Object System.Net.WebClient

    Try {
    	$client.DownloadFile($link, $outputFile)        
    }
    Catch {
        Write-Host "Error downloading: $link`r`n" -ForegroundColor Red
    } 

	$i++
}

# And finally, an on-screen message to end it all
Write-Host "Download complete. Files saved in:" -ForegroundColor DarkCyan
Write-Host $publicationPath -ForegroundColor DarkCyan
