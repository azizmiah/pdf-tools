# Housekeeping
Clear-Host
$pshost               = Get-Host
$pswindow             = $pshost.UI.RawUI
$pswindow.WindowTitle = "Image to PDF"

# Load all modules that we will need
Import-Module -Name ($PSScriptRoot + ("\modules\New-PdfFromImages.psm1")) -Force
Import-Module -Name ($PSScriptRoot + ("\modules\Compress-Images.psm1")) -Force
Import-Module -Name ($PSScriptRoot + ("\modules\Unlock-Pdf.psm1")) -Force

# Prompt user to drag the folder containing files to be merged
Write-Host "Drag a folder into this window to create a PDF" -ForegroundColor DarkCyan
$inputPath = (Read-Host).Trim() -Replace '"', ''

# Exit if the input path is not for a directory 
If (! ((Get-Item $inputPath) -is [System.IO.DirectoryInfo]) ) {
    Write-Host "Error: Invalid input" -ForegroundColor DarkCyan
    Exit
}

# Cycle through directory and convert any non-PNG files to PNG format
Write-Host "`r`n"
Write-Host "Verifying image files, please wait." -ForegroundColor DarkCyan
Write-Host "`r`n"
foreach( $file in (Get-ChildItem $inputPath -Exclude *.png) ) {
    
    $originalFile = $file.fullName
    $newFile      = $inputPath + "\" + [IO.Path]::GetFileNameWithoutExtension($originalFile) + ".png"

    # Update variables to be wrapped in quote marks to handle paths with spaces
    # or else the command to merge will fail
    $originalFile = "`"" + $originalFile + "`""
    $newFile      = "`"" + $newFile      + "`""

    $convertCommand = ".\bin\graphicsmagick-1.3.25-q16-win32\gm convert $originalFile $newFile"
    Invoke-Expression $convertCommand *>$NULL

    # Delete the original non-PNG file
    Remove-Item -Path ($file.fullName) -Force -Recurse
}

# Ask user if they wish to compress
# Calculate file size of all the images we've generated
# Ask user if they want to compress the images
$totalPages = ( Get-ChildItem $inputPath *.png ).Count
$fileSize   = ((Get-ChildItem ($inputPath + '\*.png') | Measure-Object -Sum Length).Sum / 1MB).toString("0.00")
Write-Host ("Verification complete. There are " + $totalPages + " files with a total file size of " + $filesize + "MB") -ForegroundColor Red
Write-Host "Would you like to compress your images? Enter Y or N only." -ForegroundColor Red
$compressDecision = Read-Host
$compressDecision = $compressDecision.Trim().ToLower()

If ($compressDecision -eq "yes" -or $compressDecision -eq "y") {
    Write-Host `r`n
    Write-Host "pngquant can compress images and overwrite the original." -foreground "red"
    Write-Host "There are 11 levels of compression. 1 will compress the most/slowest and 11 will compress the least/quickest." -foreground "red"
    Write-Host "The default is level 3. Enter your level of compression" -foreground "red"
    
    try {
        [int]$compressionLevel = Read-Host
    } catch {
        [int]$compressionLevel = 3
    }

    if ($compressionLevel -gt 11 -or $compressionLevel -lt 1) {
        $compressionLevel = 3
    }

    Compress-Images -path $inputPath -compressionLevel $compressionLevel

    # Compression is now complete so compare old file size with new file size
    # Output results of compression
    $oldSize  = $filesize
    $fileSize = ((Get-ChildItem ($inputPath + '\*.png') | Measure-Object -Sum Length).Sum / 1MB).toString("0.00")
    Write-Host `r`n
    Write-Host ("Images have been compressed from " + $oldSize + "MB to " + $filesize + "MB.") -ForegroundColor Red
}

# Build PDF
$pubName = "Image to PDF"
New-PdfFromImages -path $inputPath -fileName $pubName

# Unlock PDF
Unlock-Pdf -path $inputPath -fileName $pubName *>$NULL
Remove-Item ($inputPath + "\" + $pubName + ".pdf")

# Reset window title now that process is complete
$pswindow.WindowTitle = "Image to PDF"

# And finally, an on-screen message to end it all
Write-Host "`r`n"
Write-Host "PDF created." -ForegroundColor DarkCyan
