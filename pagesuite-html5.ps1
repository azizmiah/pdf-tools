# Housekeeping
Clear-Host
$pshost               = Get-Host
$pswindow             = $pshost.UI.RawUI
$pswindow.WindowTitle = "Pagesuite HTML5 downloader"

# Load all modules that we will need
Import-Module -Name ($PSScriptRoot + ("\modules\Remove-StringSpecialCharacter.psm1")) -Force
Import-Module -Name ($PSScriptRoot + ("\modules\Merge-Pdfs.psm1")) -Force
Import-Module -Name ($PSScriptRoot + ("\modules\Unlock-Pdf.psm1")) -Force

# Publication name
Write-Host "Enter the output file name" -ForegroundColor DarkCyan
[string]$pubName = Read-Host

# Publication URL
Write-Host "`r`n"
Write-Host "Enter the edition ID (eid or edid)." -ForegroundColor DarkCyan
Write-Host "For example, the edition ID in the following URL" -ForegroundColor DarkCyan
Write-Host "http://edition.pagesuite-professional.co.uk//launch.aspx?eid=1a87e2d4-3bfd-460c-83e3-5b4fad438ec6" -ForegroundColor DarkCyan
Write-Host "is" -ForegroundColor DarkCyan
Write-Host "1a87e2d4-3bfd-460c-83e3-5b4fad438ec6" -ForegroundColor DarkCyan
[string]$editionId = Read-Host

# Fetch XML manifest file and determine information such as page count and pubid
$xmlManifest = Invoke-RestMethod ("http://edition.pagesuite-professional.co.uk/get_exml.aspx?edid=" + $editionId)
$totalPages  = $xmlManifest.edition.pages.page.count
$eid         = $xmlManifest.edition.eid
$pubid       = $xmlManifest.edition.pubid

# Use the pubid & eid to get the JSON manifest
$jsonManifest = Invoke-RestMethod ("http://edition.pagesuite.com/html5/reader/get_page_groups_from_eid.aspx?pubid=" + $pubid + "&eid=" + $eid)

# Change window title to something more informative
$pswindow.WindowTitle = $pubName + " (Total pages: $totalPages)"

# Create a folder for all activity that takes place based on the folder name
[string]$publicationPath = $PSScriptRoot + '\' + ((Get-Date).ToString('HHmmss'))  + '_' + (Remove-StringSpecialCharacter -String $pubName)
New-Item -ItemType Directory -Force -Path $publicationPath | Out-Null

For ($i = 1; $i -le $totalPages; $i++) {
    
    # Location of each page and the filename for each page and then download
    [string]$pageUrl             = $jsonManifest.pageGroups[$i-1].pages.pdf
    [string]$downloadDestination = $publicationPath + '\' + ($i.ToString("000")) + ".pdf"

    (New-Object Net.WebClient).DownloadFile($pageUrl, ($downloadDestination))

    # Progress bar for downloading pages
    [string]$progressPercentage = "Downloading page $i of $totalPages ~ " + ("{0:P1}" -f ((($i / $totalPages)), 0))
    Write-Progress -Activity "Downloading $pubName" `
        -Status $progressPercentage `
        -PercentComplete (($i / $totalPages) * 100) `
        -CurrentOperation "$pageUrl" `
}

# Scan directory for PDFs
$ListOfFiles = Get-ChildItem -Path ($publicationPath + "\*.pdf") `
    | Sort-Object { [regex]::Replace($_.Name, '\d+', { $args[0].Value.PadLeft(20) }) }

# Build up list of files for the merge command
ForEach ($pdf in $ListOfFiles) {
    $commandListOfFiles += ("`"" + $pdf.FullName + "`"") + (" `"" + "1-z"  + "`" ")
}

# Call to merge
Merge-Pdfs -targetDirectory $publicationPath -inputFiles $commandListOfFiles -outputFile $pubName

# Unlock PDF
Unlock-Pdf -path $publicationPath -fileName $pubName *>$NULL

# Move PDF from working directory to applicaton directory and delete working directory
Move-Item -Path ($publicationPath + '\*ul.pdf') -Destination $PSScriptRoot -Force -ErrorAction SilentlyContinue
Remove-Item -Path $publicationPath -Force -Recurse

# Reset window title now that process is complete
$pswindow.WindowTitle = "Pagesuite HTML5 downloader"

# And finally, an on-screen message to end it all
Write-Host "`r`n"
Write-Host "This publication is now complete" -ForegroundColor DarkCyan
