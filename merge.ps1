# Housekeeping
Clear-Host
$pshost   = Get-Host
$pswindow = $pshost.UI.RawUI
$pswindow.WindowTitle = "PDF merger"

# Load all modules that we will need
Import-Module -Name ($PSScriptRoot + ("\modules\Merge-Pdfs.psm1")) -Force
Import-Module -Name ($PSScriptRoot + ("\modules\Unlock-Pdf.psm1")) -Force

# Prompt user to drag the folder containing files to be merged
Write-Host "Drag a folder into this window to merge the files within" -ForegroundColor DarkCyan
$inputPath = (Read-Host).Trim() -Replace '"', ''

# Exit if the input path is not for a directory 
If (! ((Get-Item $inputPath) -is [System.IO.DirectoryInfo]) ) {
    Exit
}

# Scan directory for PDFs
$ListOfFiles = Get-ChildItem -Path ($inputPath + "\*.pdf") `
    | Sort-Object { [regex]::Replace($_.Name, '\d+', { $args[0].Value.PadLeft(20) }) }

# Stop if no PDFs present
If ( ($ListOfFiles.count) -le 1 ) {
    Exit
}

# Build up list of files for the merge command
ForEach ($pdf in $ListOfFiles) {
    $commandListOfFiles += ("`"" + $pdf.FullName + "`"") + (" `"" + "1-z"  + "`" ")
}

# Name of output file
$outputName = ( Split-Path  $inputPath -Leaf ) + ".merged"

# Call to merge
Merge-Pdfs -targetDirectory $inputPath -inputFiles $commandListOfFiles -outputFile $outputName

# Check the output file exists to verify successful merge operation
# If successful, unlock the file and delete the original merged file
If ( Test-Path ($inputPath + "\" + $outputName + ".pdf") ) {
    Unlock-Pdf -path $inputPath -fileName $outputName *>$NULL
    Remove-Item ($inputPath + "\*.merged.pdf")
    Write-Host "`nSUCCESS: Merge and unlock successful`n" -ForegroundColor Green
} Else {
    Write-Host "ERROR: Something went wrong`n" -ForegroundColor Red
}

# Reset window title now that process is complete
$pswindow.WindowTitle = "PDF merger"
