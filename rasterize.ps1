# Housekeeping
Clear-Host
$pshost               = Get-Host
$pswindow             = $pshost.UI.RawUI
$pswindow.WindowTitle = "PDF rasterizer"

# Load all modules that we will need
Import-Module -Name ($PSScriptRoot + ("\modules\Unlock-Pdf.psm1")) -Force
Import-Module -Name ($PSScriptRoot + ("\modules\Export-PdfAsImage.psm1")) -Force
Import-Module -Name ($PSScriptRoot + ("\modules\Compress-Images.psm1")) -Force
Import-Module -Name ($PSScriptRoot + ("\modules\New-PdfFromImages.psm1")) -Force

# Prompt user to drag and drop either a file
Write-Host "Drag a PDF file into this window to rasterize it" -ForegroundColor DarkCyan
$inputPath = (Read-Host).Trim() -Replace '"', ''

# Exit if the input path is for a directory or if it is a file but one without a PDF extension 
If ( ((Get-Item $inputPath) -is [System.IO.DirectoryInfo]) -Or (([IO.Path]::GetExtension($inputPath)) -ne ".pdf") ) {
    Exit
}

# Create a folder for all activity that takes place based on the publication name
[string]$extractionPath = $PSScriptRoot + '\' + ((Get-Date).ToString('HHmmss'))  + '_rasterize'
New-Item -ItemType Directory -Force -Path $extractionPath | Out-Null

# New working directory and then render/extract each page as image using mutools
Write-Host "`nStage 1 of 3: Extracting pages`n" -ForegroundColor Blue
New-Item $extractionPath -Type Directory -Force | Out-Null
cd $extractionPath
Export-PdfAsImage -path $inputPath -resolution 175 -height 1800 -outputElsewhere $TRUE
cd ../

# Calculate file size of all the images we've generated and then ask user if they want to compress the images
Write-Host "`nStage 2 of 3: Compress pages`n" -ForegroundColor Blue

$totalPages = ( Get-ChildItem -Path ($extractionPath + "\*.png") ).count

$fileSize = ( (Get-ChildItem ($extractionPath + '\*.png') | Measure-Object -Sum Length).Sum / 1MB ).toString("0.00")
Write-Host ("Converted " + $totalPages + " files with a total file size of " + $filesize + "MB") -ForegroundColor Red
Write-Host "Would you like to compress your images? Enter Y or N only." -ForegroundColor Red
$compressDecision = Read-Host
$compressDecision = $compressDecision.Trim().ToLower()

If ($compressDecision -eq "yes" -or $compressDecision -eq "y") {
    Write-Host `r`n
    Write-Host "pngquant can compress images and overwrite the original." -foreground "red"
    Write-Host "There are 11 levels of compression. 1 will compress the most/slowest and 11 will compress the least/quickest." -foreground "red"
    Write-Host "The default is level 3. Enter your level of compression" -foreground "red"
    
    try {
        [int]$compressionLevel = Read-Host
    } catch {
        [int]$compressionLevel = 3
    }

    if ($compressionLevel -gt 11 -or $compressionLevel -lt 1) {
        $compressionLevel = 3
    }

    Compress-Images -path $extractionPath -compressionLevel $compressionLevel

    # Compression is now complete so compare old file size with new file size
    # Output results of compression
    $oldSize = $filesize
    $fileSize = ((Get-ChildItem ($extractionPath + '\*.png') | Measure-Object -Sum Length).Sum / 1MB).toString("0.00")
    Write-Host `r`n
    Write-Host ("Images have been compressed from " + $oldSize + "MB to " + $filesize + "MB.") -ForegroundColor Red
}

# Build PDF
Write-Host "`nStage 3 of 3: Building PDF`n" -ForegroundColor Blue

$pubName = [IO.Path]::GetFileNameWithoutExtension($inputPath) + '.RASTERIZED'
New-PdfFromImages -path $extractionPath -fileName $pubName

# Unlock PDF
Unlock-Pdf -path $extractionPath -fileName $pubName *>$NULL

# Move PDF from working directory to applicaton directory and delete working directory
Move-Item -Path ($extractionPath + '\*ul.pdf') -Destination $PSScriptRoot -Force -ErrorAction SilentlyContinue
Remove-Item -Path $extractionPath -Force -Recurse

# Reset window title now that process is complete
$pswindow.WindowTitle = "PDF rasterizer"

# And finally, an on-screen message to end it all
Write-Host "`r`n"
Write-Host "PDF has been rasterized" -ForegroundColor DarkCyan

# Command to find number of pages in PDF
# qpdf --show-npages FILENAME.pdf
#