# Housekeeping
Clear-Host
$pshost               = Get-Host
$pswindow             = $pshost.UI.RawUI
$pswindow.WindowTitle = "PDF unlocker"

# Load all modules that we will need
Import-Module -Name ($PSScriptRoot + ("\modules\Test-IfPdfLocked.psm1")) -Force
Import-Module -Name ($PSScriptRoot + ("\modules\Unlock-Pdf.psm1")) -Force

# Prompt user to drag and drop either a file
Write-Host "Drag a PDF file into this window to unlock it" -ForegroundColor DarkCyan
$inputPath = (Read-Host).Trim() -Replace '"', ''

# Exit if the input path is for a directory or if it is a file but one without a PDF extension 
If ( ((Get-Item $inputPath) -is [System.IO.DirectoryInfo]) -Or (([IO.Path]::GetExtension($inputPath)) -ne ".pdf") ) {
    Exit
}

# Variables for path to file (excluding filename) and variable for the name of the file without extension
# These will then be passed through to the Unlock-Pdf method
$pathToPdfWithoutTrailingSlash = [System.IO.Path]::GetDirectoryName($inputPath).TrimEnd('\')
$justFileName                  = [System.IO.Path]::GetFileNameWithoutExtension($inputPath)

# Check if PDF is password protected or not
# If the test returns a code 0, the PDF has no password. Proceed with unlocking
# If the test returns a code 2, the PDF is protected. Ask user for password and then unlock
If ( (Test-IfPdfLocked -pathToFile $inputPath) -Eq 0) {
  
    # We know the PDF has no password on it so let's proceed with unlocking
    $result = Unlock-Pdf -path $pathToPdfWithoutTrailingSlash -fileName $justFileName

    # Success/Error message depending on the status code QPDF exits with 
    If($result -Eq 0) {
        Write-Host "`nSUCCESS: PDF unlocked!`n" -ForegroundColor Green               
    } Else {
        Write-Host "`nERROR(NPW): Something happened and the PDF could not be unlocked`n" -ForegroundColor Red        
    }

} Else {

    # The PDF has a password so let's ask for it and then unlock
    Write-Host "`nEnter the PDF password" -ForegroundColor Yellow
    $password = Read-Host

    # Pass the PDF password as a paramter in order to unlock the file
    $result = Unlock-Pdf -path $pathToPdfWithoutTrailingSlash -fileName $justFileName -isPasswordProteced $TRUE -password $password

    # Success/Error message depending on the status code QPDF exits with 
    If($result -Eq 0) {
        Write-Host "`nSUCCESS: PDF unlocked!`n" -ForegroundColor Green               
    } Else {
        Write-Host "`nERROR(YPW): Something happened and the PDF could not be unlocked`n" -ForegroundColor Red        
    }
}

# Reset window title now that process is complete
$pswindow.WindowTitle = "PDF unlocker"
