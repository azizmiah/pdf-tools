# pdf-tools #

A set of PowerShell scripts that help dealing with PDF related stuff.

### What scripts does this repository contain? ###

* Download/convert Yudu and Pagesuite titles
* Unlock, merge and rasterize PDF files
* Batch image downloader

----

*https://bitbucket.org/azizmiah/pdf-tools*