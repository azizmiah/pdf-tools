Function Test-IfPdfLocked {
    Param ([string]$pathToFile)

    $checkCommand = ".\bin\qpdf-6.0.0\qpdf --show-encryption " + ('"' + $pathToFile + '"')
    Invoke-Expression $checkCommand *>$NULL

    Return $LASTEXITCODE
}

Export-ModuleMember -Function Test-IfPdfLocked