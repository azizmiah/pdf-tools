Function Merge-Pdfs {
    Param ([string]$targetDirectory, [string]$inputFiles, [string]$outputFile)

    $outputFile = $targetDirectory + "\" + $outputFile + ".pdf"

    $command = ".\bin\qpdf-6.0.0\qpdf --empty --pages " + $inputFiles + "-- " + ("`"" + $outputFile + "`"")
    Invoke-Expression $command *>$NULL

}

Export-ModuleMember -Function Merge-Pdfs