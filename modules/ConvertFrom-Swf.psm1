Function ConvertFrom-Swf {
    Param ([string]$path, [int]$totalPages)
    
    # Launch the converter
    Invoke-Item '.\bin\kurst-cameyo\Kurst SWF Renderer with Adobe AIR.cameyo.exe'

    # Recommended settings
    Write-Host `r`n
    Write-Host "The conversion application is now being launched. `nThese are the recommended settings: " -ForegroundColor DarkCyan
    Write-Host "Frame rate: `t `t 1" -ForegroundColor DarkCyan
    Write-Host "Content scale: `t `t 2.0 to 3.0 (may need to compress if high)" -ForegroundColor DarkCyan
    Write-Host "Stop exporting: `t after 1 second `n" -ForegroundColor DarkCyan
    
    # Detect each successfully converted page and show a progress bar
    For ($i=1; $i -le $totalPages; $i++) {
        $filePath = $path + "\" + $i.ToString("000") + "\*.png"
        [bool]$evaluatePath = Test-Path $filePath

        # Calculate the percentage of links which have been downloaded for the progress bar
        # The percentage is formatted as a percentage to 1 decimal place - hence {0:P1}
        [string]$progressPercentage = ("{0:P1}" -f (((($i-1) / $totalPages)), 0)) + " complete"
        Write-Progress -Activity "Converting pages from SWF to PNG" `
            -Status $progressPercentage `
            -PercentComplete ((($i-1) / $totalPages) * 100) `
            -CurrentOperation  "Now converting page $i of $totalPages"

        While (-Not $evaluatePath) {
            If (Test-Path $filePath) {
                Break
            }
        }
    }
}

Export-ModuleMember -Function ConvertFrom-Swf 