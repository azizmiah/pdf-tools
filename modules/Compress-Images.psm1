Function Compress-Images {
    Param ([string]$path, [int]$compressionLevel)

    # Scan directory to count the number of PNG files in the publication directory
    $listOfImages = Get-ChildItem -Path ($path + "\*.png") | Sort Name

    # Keep track of every image compressed
    $counter = 0

    ForEach ($image in $listOfImages) {

        # Compression each image at the given compression level
        Invoke-Expression (".\bin\pngquant-2.7.2-windows\pngquant.exe --skip-if-larger --ext=.png --force --speed $compressionLevel `"" + $image.FullName + "`"")

        # Progress bar for compression
        [string]$progressPercentage = ("{0:P1}" -f (((($counter+1) / $listOfImages.Count)), 0)) + " complete"
        Write-Progress -Activity "Compressing images. This may take a while." `
            -Status $progressPercentage `
            -PercentComplete (($counter / $listOfImages.Count) * 100)
            # -CurrentOperation "Compressing image " + ($counter+1) + " of " + ($listOfImages.Count) 

        $counter++;
    }
}

Export-ModuleMember -Function Compress-Images 