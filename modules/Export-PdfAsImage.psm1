Function Export-PdfAsImage {
    Param ([string]$path, [string]$resolution, [string]$height, [Boolean]$outputElsewhere)

    # If the $outputElsewhere flag is set to TRUE, we will export in a subdirectory of the project path
    # and if this is the case, the path to mutool needs to go up one level more to reach the executable
    If ($outputElsewhere -eq $TRUE) {    
        $command = ".\..\bin\mupdf-1.10a-windows\mutool draw -o %d.png -r $resolution -h $height " + ("`"" + $path + "`"")
    } Else {
        $command = ".\bin\mupdf-1.10a-windows\mutool draw -o %d.png -r $resolution -h $height " + ("`"" + $path + "`"")        
    }

    Invoke-Expression ($command) *>$NULL
}

Export-ModuleMember -Function Export-PdfAsImage 

# mudraw version 1.10a
# Usage: mudraw [options] file [pages]
#         -p -    `password

#         -o -    output file name (%d for page number)
#         -F -    output format (default inferred from output file name)
#                 raster: png, tga, pnm, pam, pbm, pkm, pwg, pcl, ps
#                 vector: svg, pdf, trace
#                 text: txt, html, stext

#         -s -    show extra information:
#                 m - show memory use
#                 t - show timings
#                 f - show page features
#                 5 - show md5 checksum of rendered image

#         -R -    rotate clockwise (default: 0 degrees)
#         -r -    resolution in dpi (default: 72)
#         -w -    width (in pixels) (maximum width if -r is specified)
#         -h -    height (in pixels) (maximum height if -r is specified)
#         -f -    fit width and/or height exactly; ignore original aspect ratio
#         -B -    maximum band_height (pgm, ppm, pam, png output only)
#         -T -    number of threads to use for rendering (banded mode only)

#         -W -    page width for EPUB layout
#         -H -    page height for EPUB layout
#         -S -    font size for EPUB layout
#         -U -    file name of user stylesheet for EPUB layout

#         -c -    colorspace (mono, gray, grayalpha, rgb, rgba, cmyk, cmykalpha)
#         -G -    apply gamma correction
#         -I      invert colors

#         -A -    number of bits of antialiasing (0 to 8)
#         -A -/-  number of bits of antialiasing (0 to 8) (graphics, text)
#         -l -    minimum stroked line width (in pixels)
#         -D      disable use of display list
#         -i      ignore errors
#         -L      low memory mode (avoid caching, clear objects after each page)
#         -P      parallel interpretation/rendering

#         -y l    List the layer configs to stderr
#         -y -    Select layer config (by number)
#         -y -{,-}*       Select layer config (by number), and toggle the listed entries

#         pages   comma separated list of page numbers and ranges