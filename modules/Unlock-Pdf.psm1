Function Unlock-Pdf {
    Param ([string]$path, [string]$fileName, [Boolean]$isPasswordProteced, [string]$password)

    $inputFile  = $path + "\" + $fileName + ".pdf"
    $outputFile = $path + "\" + $fileName + ".ul.pdf"

    # If the method was called with a password, decrypt with the password
    If($isPasswordProteced -eq $FALSE) {
        $unlockCommandOptions = ".\bin\qpdf-6.0.0\qpdf.exe --decrypt "        
    } Else {
        $unlockCommandOptions = ".\bin\qpdf-6.0.0\qpdf --password=" + ("`"" + $password + "`"") + " --decrypt "
    }

    $unlockCommandInFile  = ("`"" + $inputFile + "`"") + " "
    $unlockCommandOutFile = ("`"" + $outputFile + "`"")

    # Write the command's output to NULL to suppress any messages
    Invoke-Expression ($unlockCommandOptions + $unlockCommandInFile + $unlockCommandOutFile) *>$NULL
    
    Return $LASTEXITCODE
}

# Command to find number of pages in PDF
# qpdf --show-npages FILENAME.pdf

Export-ModuleMember -Function Unlock-Pdf 