Function New-PdfFromImages {
    Param ([string]$path, [string]$fileName)
    
    # Load iTextSharp and System.Drawing
    $iTextSharpFilePath  = ".\bin\itextsharp-all-5.5.9\itextsharp.dll"
    Add-Type -Path ($iTextSharpFilePath)
    Add-Type -AssemblyName System.Drawing

    # Get all of the images in the folder
    $Images =   Get-ChildItem -Path $path `
                              -Recurse `
                              -Include ("*.jpg", "*.png") `
                | Sort-Object { [regex]::Replace($_.Name, '\d+', { $args[0].Value.PadLeft(20) }) }

    # Set up our filename
    $fileName = $path + "\" + $fileName + ".pdf"

    # Create our stream, document and bind a writer
    $fileStream = New-Object System.IO.FileStream(($fileName), [System.IO.FileMode]::Create)
    $doc = New-Object iTextSharp.text.Document
    $writer = [iTextSharp.text.pdf.PdfWriter]::GetInstance($doc, $filestream)

    # Open the document for writing
    [void]$doc.Open()

    ## Remove all document margins
    [void]$doc.SetMargins(0, 0, 0, 0)

    # Loop through each image in the folder
    foreach($Image in $Images)
    {
        # Create a .Net image so that we can get the image dimensions
        $bmp = New-Object System.Drawing.Bitmap($image.FullName)

        ##Create an iTextSharp rectangle that corresponds to those dimensions
        $rect = New-Object iTextSharp.text.Rectangle($bmp.Width, $bmp.Height)

        # Set the next page size to those dimensions and add a new page
        [void]$doc.SetPageSize( $rect )
        [void]$doc.NewPage() 

        # Add our image to the page
        [void]$doc.Add([iTextSharp.text.Image]::GetInstance( $image.FullName ))

        ##Cleanup
        [void]$bmp.Dispose()
    }

    # Cleanup
    $doc.Close()
    $doc.Dispose()
    $writer.Dispose()

}

# Credits
# http://stackoverflow.com/questions/24450351/powershell-and-itextsharp-add-multiple-images-to-pdf

Export-ModuleMember -Function New-PdfFromImages 

