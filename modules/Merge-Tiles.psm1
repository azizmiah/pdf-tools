Function Merge-Tiles {
    Param ([string]$inputFiles, [string]$outputFiles, [string]$type)

    If ($type -eq "horizontally") {
        $mergeCommand = ".\bin\graphicsmagick-1.3.25-q16-win32\gm convert $inputFiles +append $outputFiles"
    } Else { # vertically
        $mergeCommand = ".\bin\graphicsmagick-1.3.25-q16-win32\gm convert $inputFiles -append $outputFiles"
    }

    Invoke-Expression $mergeCommand *>$NULL

}

Export-ModuleMember -Function Merge-Tiles 